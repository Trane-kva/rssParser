import requests
import logger
import sys
import xml.etree.ElementTree as ET
from datetime import datetime

records = []
lastRecord = None

logging = logger.createLogger("lostfilmRSSProcessor.py", 'log.log')


class Entry:
    """
    A class that is used for store parsed information about series.datetime.
    title - Series title
    url - torrent location path
    posterUrl - poster location image
    fileName - file name for telegram bot
    """
    title = None
    url = None
    posterURL = None
    fileName = None

    def __init__(self, title, url, posterURL):
        self.title = title
        self.url = url
        self.posterURL = posterURL

    # Reloaded method for converting object to string
    def __str__(self):
        if not self.fileName:
            return "Poster: " + self.posterURL + "\n" + "Title: " +\
                self.title + "\n" + "URL: " + self.url + "\n"
        else:
            return "Poster: " + self.posterURL + "\n" + "Title: " + \
                   self.title + "\n" + "URL: " + self.url + "\n" + \
                   "File Name: " + self.fileName + "\n"


# Used for getting RSS feed as XML file.
def getHTML(url):
    response = requests.get(url)
    response.encoding = 'utf-8'
    return response.text


# Used for getting series from the list of desired series
def processXML(config, downloadList):
    global records
    global lastRecord
    biggestDate = None
    """
    Loading latest record that was added to download list
    from 'lastDownloaded file'
    """
    loadFromDateFile()
    biggestDate = lastRecord

    """
    Trying to get xml file of RSS feed if all ok adding it to xml parser.
    If not - exitting program.
    """
    try:
        xml = getHTML(config["website"]["rssLink"])
        content = ET.fromstring(xml)
    except ET.ParseError:
        logging.error("Some troubles with returned xml file")
        sys.exit(1)

    """
    Iterating through series list in rss feed and looking for desired series
    listed in config file.
    """
    for record in content.iter('item'):
        # Getting english name from item element inside xml file and checking if
        # it inside desired series list
        if record[0].text.split(").")[0].split(" (")[1] in downloadList:
            seriesKey = record[0].text.split(").")[0].split(" (")[1]
            # Checking if series is a new one and hasn't downloaded yet.
            if convertDate(record[2].text) > lastRecord:
                # Checking if series quality is meet requirements listed in conf
                if record[1].text == config['series']['quality']:
                    # Creating Entry object with parsed info and adding it into
                    # records list.
                    records.append(Entry(record[0].text, record[3].text,
                                   downloadList[seriesKey]['img']))
                    if convertDate(record[2].text) > biggestDate:
                        # Checking if series update date is the newliest one
                        # if so, updating date
                        biggestDate = convertDate(record[2].text)
    # Updating file with the newliest date
    updateDateFile(str(biggestDate))


def convertDate(date):
    """ Used for converting date in string format into the datetime object. """
    date = date.split(" ")
    date = date[3] + "-" + date[2] + "-" + date[1] + " " + date[4]
    date = datetime.strptime(date, '%Y-%b-%d %H:%M:%S')
    return date


def generatePosterURL(config, seriesKey):
    """ Used for generating poster url based on
    series id listed in config file. """
    try:
        s_id = config['series']['list'][seriesKey]['id']
        return "http://static.lostfilm.tv/Images/" + s_id + "/Posters/poster.jpg"
    except KeyError:
        logging.warning("Series %s don't have id" % config['series']['list'][seriesKey])
        return "http://static.lostfilm.tv/Images/" + '0' + "/Posters/poster.jpg"


def loadFromDateFile():
    """ Just loads information from 'lastDownloaded' file and converting it into
        datetime object. """
    global lastRecord
    try:
        with open("lastDownloaded", "r") as date:
            fileInfo = date.read().strip()
            if fileInfo == "":
                # If no date in lastDownloaded, getting today 00:00:00
                lastRecord = datetime.combine(datetime.now().date(),
                                              datetime.now().time().min)
            else:
                lastRecord = datetime.strptime(fileInfo, '%Y-%m-%d %H:%M:%S')
    except IOError:
        logging.warning('Cannot open "lastDownloaded" file', exc_info=True)
        # Getting today 00:00:00
        lastRecord = datetime.combine(datetime.now().date(),
                                      datetime.now().time().min)


def updateDateFile(date):
    """ Writing new date to the 'lastDownloaded' file. """
    with open("lastDownloaded", "w") as dateFile:
        dateFile.write(date)
