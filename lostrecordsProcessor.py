import requests
import logger
import sys

session = None
logging = logger.createLogger('lostrecordsProcessor.py', 'log.log')


def loginToWebSite(cookies):
    """
    Basically creates session for downloading torrent files and adding
    cookies required for authentification
    """
    global session
    session = requests.Session()
    requests.utils.add_dict_to_cookiejar(session.cookies, cookies)


def downloadFile(url, location, fileName):
    global session
    """
    Trying to get torrent file as binary data.
    Success - writing it to the file which name is generated from series title.
    Fail - finishing program execution.
    """
    torrentRAW = session.get(url, stream=True)
    if torrentRAW.headers['Content-Type'] in 'text/html;charset=utf-8':
        logging.error("Cookies information in config.yml is wrong!")
        sys.exit(1)
    fileDir = location + fileName + '.torrent'
    try:
        torrent = open(fileDir, 'wb')
        for chunk in torrentRAW.iter_content(chunk_size=128):
            torrent.write(chunk)
        torrent.close()
    except IOError:
        logging.error('torrentsLocationPath in config.yml is not exists!', exc_info=True)
        sys.exit(1)


def getTorrents(records, config):
    """
    Some methot encapsulation for pretty looking code. 
    """
    loginToWebSite(config["website"]["cookies"])
    for record in records:
        downloadFile(record.url, config['location']['torrentsLocationPath'],
                     record.title)
    logging.info("getTorrents - Downloaded %s torrent files" % len(records))
