import yaml
import sys

import logger
import lostrecordsProcessor as lp
import lostfilmRSSProcessor as lost
import transmissionModule as tm
import webSiteScrapper as ws
from telegramBot import TelegramBot

__author__ = "Vladislav Kulymbetov "
__credits__ = "Vladislav Kulymbetov"
__version__ = "1.4.1"

logging = logger.createLogger("main.py", 'log.log')


def main():
    # Loading config
    config = loadConfigFile()

    # Getting downloadList from website favorites
    downloadList = ws.getDownloadList(config)
    # Getting series list from Lostfilm RSS
    lost.processXML(config, downloadList)
    # Downloading torrent files and adding them into transmission daemon
    if len(lost.records) > 0:
        lp.getTorrents(lost.records, config)
        tm.addTorrents(config, lost.records)
        # Creating TelegramBot
        InformerBot = TelegramBot(config)
        for record in lost.records:
            # Sending information about each series added
            InformerBot.sendUpdateMessage(record, 'html')
    tm.deleteFinished(config)
    logging.info("Script successfully finished")


def loadConfigFile():
    """Used for opening yml config file and parsing it to the py dict format"""
    try:
        with open('config.yml', encoding='utf-8') as configFile:
            config = yaml.load(configFile)
        logging.info("config.yml loaded successfully")
        checkConfig(config)
        return config
    except IOError:
        logging.error('Cannot open cofig.yml', exc_info=True)
        sys.exit(1)


def checkConfig(config):
    """Some config checks for normal script work."""
    if "website" not in config:
        logging.error("config.yml don't have website section!")
        sys.exit(1)
    if "location" not in config:
        logging.error("config.yml don't have location section!")
        sys.exit(1)
    if "botSettings" not in config:
        logging.error("config.yml don't have botSettings section!")
        sys.exit(1)
    if "transmission" not in config:
        logging.error("config.yml don't have transmission section!")
        sys.exit(1)
    if "series" not in config:
        logging.error("config.yml don't have series section!")
        sys.exit(1)
    if config['series']['list'] is None:
        logging.error("config.yml don't have any series in series: "
                      "list: section!")
        sys.exit(1)


if __name__ == '__main__':
    main()
