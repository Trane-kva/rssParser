import requests
import json
import logger


class TelegramBot:
    apiUrl = None
    chatId = None
    messageData = {}

    logging = logger.createLogger("telegramBot.py", "log.log")

    def __init__(self, config):
        """If bot notifications are enabled - loading info from config dict"""
        if config['botSettings']['enable']:
            self.apiUrl = config['botSettings']['apiUrl'] +\
                          config['botSettings']['token']
            self.chatId = config['botSettings']['chat_id']
            self.logging.info('Bot created')
        else:
            pass

    def sendMessage(self, message, parseMode):
        """
        Generating message body and sending it to the telegram chat using
        Telegram Bot API
        """
        methodURL = self.apiUrl + "/sendMessage"
        self.messageData['chat_id'] = self.chatId
        self.messageData['parse_mode'] = str(parseMode)
        self.messageData['text'] = str(message)
        result = requests.post(methodURL, data=self.messageData)
        if result.status_code != 200:
            self.logging.error("Couldn't send message. \n %s" % result.text)
        else:
            response = json.loads(result.text)
            if not response['ok']:
                self.logging.error("Server refused message. \n %s" % response)
        self.messageData = {}

    def createMessageBody(self, record):
        """
        Used for formatting message into pretty format:
        Series title
        Series poster
        """
        return record.title + ' <a href="' + record.posterURL + '">&#8204</a>'

    def createButton(self, callbackData):
        """Used for creating button with callback data in it"""
        messageData = {}
        messageData['inline_keyboard'] = [[{"text": "Watched",
                                            "callback_data": callbackData}]]
        return str(json.dumps(messageData))

    def sendUpdateMessage(self, record, parseMode):
        """
        Generating message body and sending it to the telegram chat using
        Telegram Bot API
        """
        methodURL = self.apiUrl + "/sendMessage"
        messageBody = self.createMessageBody(record)
        callbackData = record.fileName
        button = self.createButton(callbackData)
        self.messageData['chat_id'] = self.chatId
        self.messageData['parse_mode'] = str(parseMode)
        self.messageData['text'] = messageBody
        self.messageData["reply_markup"] = button

        result = requests.post(methodURL, data=self.messageData)
        if result.status_code != 200:
            self.logging.error("Couldn't send message. \n %s" % result.text)
        else:
            response = json.loads(result.text)
            if not response['ok']:
                self.logging.error("Server refused message. \n %s" % response)
        self.messageData = {}

    def deleteMessage(self, message_id):
        """
        Deleting message from channel with particular IP using Telegram Bot API
        """
        methodURL = self.apiUrl + "/deleteMessage"
        self.messageData['chat_id'] = self.chatId
        self.messageData['message_id'] = message_id

        result = requests.post(methodURL, data=self.messageData)
        if result.status_code != 200:
            self.messageData = {}
            methodURL = self.apiUrl + '/editMessageText'
            self.messageData['chat_id'] = self.chatId
            self.messageData['message_id'] = message_id
            self.messageData['text'] = "(deleted)"
            self.messageData['parse_mode'] = 'html'
            result = requests.post(methodURL, data=self.messageData)
            if result.status_code != 200:
                self.logging.error("Couldn't delete and edit message. \n %s" % result.text)
        else:
            response = json.loads(result.text)
            if not response['ok']:
                self.logging.error("Server refused command. \n %s" % response)
        self.messageData = {}
