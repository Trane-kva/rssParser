import sys
import logger
import bencode
from transmissionrpc import Client
from transmissionrpc import error as TransmissionException

logging = logger.createLogger("transmissionModule.py", "log.log")

connection = None


def connect(config):
    """ Used for connecting to the transmission daemon. """
    global connection
    connection = Client(address=config['transmission']['ip'],
                        port=config['transmission']['port'],
                        user=config['transmission']['username'],
                        password=config['transmission']['password'])


def addTorrents(config, records):
    """
    Adding newly downloaded torrent files into the transmission daemon.
    """
    try:
        connect(config)
    except TransmissionException.TransmissionError as connExc:
        logging.error("Couldn't connect to Transmission Daemon. Check connection settings in config.yml")
        sys.exit(1)
    for torrentName in records:
        try:
            # Parsing torrent file for getting file name for telegram bot data
            torrentName.fileName = getTorrentFileName(config, torrentName)
            connection.add_torrent("file://" +
                                   config['location']['torrentsLocationPath']
                                   + torrentName.title + ".torrent")
            logging.info("%s.torrent was added to Transmission successfully" % torrentName.title)
        except TransmissionException.TransmissionError as connExc:
            logging.warning("%s.torrent wasn't added for some reason" %
                            torrentName.title, exc_info=True)
            continue


def getTorrentsInfo():
    """Getting torrents information for the further updates."""
    torrentList = connection.get_torrents()
    for torrent in torrentList:
        eta = None
        try:
            eta = torrent.eta
        except Exception as e:
            eta = "Stopped"

        print(torrent.name + " " + str(torrent.totalSize / 1000000000)[:4] +
              "GB " + str(torrent.percentDone * 100)[:5] + "% " +
              str(eta))


def deleteFinished(config):
    """Used for deleting finished torrents from transmission - daemon"""
    if connection is None:
        connect(config)

    torrentList = connection.get_torrents()
    for torrent in torrentList:
        if torrent.percentDone == 1:
            connection.remove_torrent(torrent.id)
            logging.info("%s.torrent was finished and removed." % torrent.name)


def getTorrentFileName(config, record):
    """Used for getting file name to download from .torrent file"""
    torrentFile = None

    # Reading .torrent file
    with open(config['location']['torrentsLocationPath'] + record.title + ".torrent", 'rb') as torrent:
        torrentFile = torrent.read()

    # Parsing using bencode library
    torrentFile = bencode.decode(torrentFile)

    # Returning file name
    return torrentFile['info']['name']