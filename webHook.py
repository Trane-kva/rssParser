import os
import sys
import yaml

from telegramBot import TelegramBot
from flask import Flask, request, abort

app = Flask(__name__)
# logging = logger.createLogger("webHook.py", 'webHookLog.log')
# config = loadConfigFile()
# bot = TelegramBot()


@app.route('/', methods=['POST'])
def webhook():
    if request.method == 'POST':
        telegramResponse = request.json
        print(telegramResponse)
        processResponseJSON(telegramResponse)
        return '', 200
    else:
        abort(400)


def processResponseJSON(jsonData):
    """
    Processing Telegram POST request.
    React only on callback_query and user with id 85889190
    """
    if "callback_query" in jsonData:
        # print(jsonData['callback_query']['data'])
        if jsonData['callback_query']['from']['id'] == 85889190:
            fileDeleted = deleteCallbackFile(jsonData['callback_query']
                                                     ['data'])
            if fileDeleted:
                bot.deleteMessage(jsonData['callback_query']['message']
                                          ['message_id'])


def deleteCallbackFile(fileToDelete):
    """Deleting from server media file with name recieved from Telegram Post"""
    success = False
    seriesLocation = config['location']['contentLocationPath']
    for fileName in os.listdir(seriesLocation):
        if fileName.startswith(fileToDelete):
            os.remove("{}/{}".format(seriesLocation, fileName))
            # logging.info("{} deleted successfully".format(fileName))
            success = True
    return success


def loadConfigFile():
    """Used for opening yml config file and parsing it to the py dict format"""
    try:
        with open('config.yml', encoding='utf-8') as configFile:
            config = yaml.load(configFile)
        # logging.info("config.yml loaded successfully")
        checkConfig(config)
        return config
    except IOError:
        # logging.error('Cannot open cofig.yml', exc_info=True)
        sys.exit(1)


def checkConfig(config):
    """Some config checks for normal script work."""
    if "webhookSettings" not in config:
        # logging.error("config.yml don't have webhookSettings section!")
        sys.exit(1)


if __name__ == '__main__':
    config = loadConfigFile()
    bot = TelegramBot(config)
    app.run(host=config['webhookSettings']['webhookIP'],
            port=config['webhookSettings']['webhookPort'],
            ssl_context=(config['webhookSettings']['certFileLocation'],
                         config['webhookSettings']['keyFileLocation']))
