import requests
import logger
import json
import sys


logging = logger.createLogger('webSiteScrapper.py', 'log.log')
session = None


def loginToWebsite(config):
    """
    Provides login to the website and stores session for further manipulations
    """
    global session
    payload = {
        'act': 'users',
        'mail': config['website']['login'],
        'pass': config['website']['password'],
        'type': 'login',
        'need_capcha': '',
        'captcha': '',
        'rem': '1',
    }
    session = requests.Session()
    response = session.post(config['website']['ajaxLink'], data=payload)
    responseDict = json.loads(response.text)
    if 'error' in responseDict:
        logging.error("Username or password are incorrect.")
        sys.exit(1)


def getFavoritesList(config):
    """
    Collects all favorite tv shows that were returned after ajax request
    from the lostfilm.tv. If there is no favorite tv shows, just log and
    exit, if there are. Collect all records by sending POST to ajax.
    """
    global session
    serials = []
    o = 0
    payload = {
        'act': 'serial',
        'type': 'search',
        'o': str(o),
        's': '3',
        't': '99'
    }
    response = session.post(config['website']['ajaxLink'], data=payload)
    responseDict = json.loads(response.text)
    if 'data' not in responseDict:
        logging.error("There is no favorite series on the website")
        sys.exit(1)
    else:
        while True:
            response = session.post(config['website']['ajaxLink'],
                                    data=payload)
            responseDict = json.loads(response.text)
            if len(responseDict['data']) == 0:
                break
            else:
                for record in responseDict['data']:
                    serials.append(record)
                o += 10
                payload['o'] = o
    return serials


def compileDownloadList(serials):
    """
    After collecting all favorite shows we need to get info that is required
    """
    downloadList = {}
    for serial in serials:
        info = {
            serial['title_orig']: {
                'name': serial['title'],
                'altName': serial['title_orig'],
                'id': serial['id'],
                'img': generatePosterLocation(serial)
            }
        }
        downloadList.update(info)
    return downloadList


def generatePosterLocation(serial):
    """Generates poster by replacing image by poster"""
    if serial['has_image']:
        return 'http:' + serial['img'].replace('image.jpg', 'poster.jpg')
    else:
        return 'http://static.lostfilm.tv/Images/0/Posters/poster.jpg'


def getDownloadList(config):
    """Encapsulating all methods into one"""
    loginToWebsite(config)
    return compileDownloadList(getFavoritesList(config))
